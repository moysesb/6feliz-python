#!/usr/bin/env python3
import signal
import sys

from fmath import fibonacci

from gi.repository import Gtk,GLib

class GraphWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Graph Canvas")

        self.canvas = GraphCanvas(600, 430)
        self.box = Gtk.Box()
        self.box.pack_start(self.canvas, True, True, 0)
        self.add(self.box)
        self.connect('check-resize', lambda s: s.canvas.resize())


class GraphCanvas(Gtk.DrawingArea):    
    def __init__(self, width, height):
        Gtk.DrawingArea.__init__(self)
        self.width = width
        self.height = height
        self.set_size_request(width,height)
        self.connect('draw', GraphCanvas.on_draw)

    def resize(self):
        self.width = self.get_allocation().width
        self.height = self.get_allocation().height
        self.queue_draw() # set_size_request(self.width, self.height)
        
    def on_draw(self, context):
        context.set_source_rgb(1.0, 1.0, 1.1)
        context.paint()
        bounds = self.draw_point_grid(context)
        self.draw_cartesian_plane(context, *bounds)
        self.draw_fibonacci_squares(context)
        context.set_line_width(2.0)
        context.set_source_rgb(0.0, 0.0, 0.0)  
        context.rectangle(50, 50, 100, 100)
        context.stroke()
        context.set_source_rgb(0.0, 0.850, 0.0)
        context.rectangle(50,50, 100, 100)
        context.fill()
        return False

    def draw_point(self, ctx, x, y, h=1.0, w=1.0):
        ctx.move_to(x, y)
        ctx.line_to(x+w, y+h)
        ctx.stroke()
        
    def draw_point_grid(self, ctx):
        ctx.set_source_rgb(0.0, 0.0, 0.0)
        ctx.set_line_width(0.5)
        last_x, last_y = (self.width, int(self.height / 10.0) * 10)
        [self.draw_point(ctx, x, y) for x in range(10, last_x, 10) for y in range(0, last_y, 10)]
        return (last_x, last_y)

    def draw_cartesian_plane(self, ctx, last_x, last_y):
        ctx.set_source_rgb(0.0, 0.0, 0.250)
        ctx.set_line_width(1.0)
        origin = (10, last_y-20)
        print(origin)
        
        def draw_x_axis():
            ctx.move_to(*origin)
            ctx.line_to(self.width - 20, origin[1])
            ctx.stroke()
            ctx.move_to(*origin)
            for x in range(10, last_x-40, 40):
                self.draw_point(ctx, x+50, origin[1], 5.0)
        def draw_y_axis():
            ctx.move_to(origin[0]+10, origin[1]+10)
            ctx.line_to(origin[0]+10, 10)
            ctx.stroke()
            for y in range(self.height-last_y-20, origin[1], 40):
                self.draw_point(ctx, 20, self.height - y - 40, 0.0, 5.0)
            
        draw_x_axis()
        draw_y_axis()

    def draw_fibonacci_squares(self, ctx):
        origin_x = self.width / 2.0
        origin_y = self.height / 2.0
        
        def draw_square(origin_x, origin_y, side):
            ctx.rectangle(origin_x - (side/2.0), origin_y - (side/2.0), side, side)

        ctx.set_source_rgb(1.0, 1.0, 1.0)
        for n in range(1, 8, 1):
            side_len = fibonacci(n) * 10
            print("Drawing square with sides %.2f at x:%.2f, y:%.2f" % (side_len, origin_x, origin_y))
            draw_square(origin_x, origin_y, side_len)
            if (n % 2) == 0:
                origin_y = origin_y - side_len
            else:
                origin_x = origin_x - side_len

if __name__ == '__main__':
    print("")
    win = GraphWindow()
    win.connect("delete-event", Gtk.main_quit)
    win.show_all()
    Gtk.main()
