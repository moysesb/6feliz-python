class memoize:
    def __init__(self, function):
        self.f = function
        self.memo = {}

    def __call__(self, arg):
        if arg not in self.memo:
            self.memo[arg] = self.f(arg)
        return self.memo[arg]
